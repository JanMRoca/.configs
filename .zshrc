# Source Prezto.
if [[ -s "${ZDOTDIR:-$HOME}/.zprezto/init.zsh" ]]; then
  source "${ZDOTDIR:-$HOME}/.zprezto/init.zsh"
fi

alias apt='sudo apt-fast'
alias u='clear; cd ../; pwd; ls -lhGgo'
alias d='clear; cd -; ls -lhGgo'
alias x='exit'
alias tmux='tmux-next'

function remloc {
	git branch --merged | egrep -v "(^\*|master|dev)" | xargs git branch -d
}

cleanswap() {
	sudo swapoff -a
	sudo swapon -a
}

cldock() {
	if [ $1 ]; then
		docker ps -a | grep "$1" | awk {'print $1'} | xargs docker rm
	else
		docker rm $(docker ps -aq -f status=exited)
	fi
}

climdoc() {
	if [ $1 ]; then
		docker images | grep "$1" | awk {'print $3'} | xargs docker rmi
	else
		docker rmi $(docker images | grep "^<none>" | awk '{print $3}')
	fi
}

# as suggested by Mendel Cooper in "Advanced Bash Scripting Guide"
extract () {
   if [ -f $1 ] ; then
       case $1 in
        *.tar.bz2)      tar xvjf $1 ;;
        *.tar.gz)       tar xvzf $1 ;;
        *.tar.xz)       tar Jxvf $1 ;;
        *.bz2)          bunzip2 $1 ;;
        *.rar)          unrar x $1 ;;
        *.gz)           gunzip $1 ;;
        *.tar)          tar xvf $1 ;;
        *.tbz2)         tar xvjf $1 ;;
        *.tgz)          tar xvzf $1 ;;
        *.zip)          unzip $1 ;;
        *.Z)            uncompress $1 ;;
        *.7z)           7z x $1 ;;
        *)              echo "don't know how to extract '$1'..." ;;
       esac
   else
       echo "'$1' is not a valid file!"
   fi
}

function mcd { mkdir -p "$1" && cd "$1";}


